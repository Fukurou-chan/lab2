#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<iostream>
#define N 10

using namespace std;
typedef unsigned char byte;

typedef struct _Node {
	void *value;
	struct _Node *next;
	struct _Node *prev;
} Node;


typedef struct _DblLinkedList {
	size_t size;
	Node *head;
	Node *tail;
} DblLinkedList;


DblLinkedList* createDblLinkedList() {
	DblLinkedList *tmp = (DblLinkedList*)malloc(sizeof(DblLinkedList));
	tmp->size = 0;
	tmp->head = tmp->tail = NULL;

	return tmp;
}


void deleteDblLinkedList(DblLinkedList **list) {
	Node *tmp = (*list)->head;
	Node *next = NULL;
	while (tmp) {
		next = tmp->next;
		free(tmp);
		tmp = next;
	}
	free(*list);
	(*list) = NULL;
}

void pushFront(DblLinkedList *list, void *data) {
	Node *tmp = (Node*)malloc(sizeof(Node));
	if (tmp == NULL) {
		exit(1);
	}
	tmp->value = data;
	tmp->next = list->head;
	tmp->prev = NULL;
	if (list->head) {
		list->head->prev = tmp;
	}
	list->head = tmp;

	if (list->tail == NULL) {
		list->tail = tmp;
	}
	list->size++;
}


void* popFront(DblLinkedList *list) {
	Node *prev;
	void *tmp;
	if (list->head == NULL) {
		exit(2);
	}

	prev = list->head;
	list->head = list->head->next;
	if (list->head) {
		list->head->prev = NULL;
	}
	if (prev == list->tail) {
		list->tail = NULL;
	}
	tmp = prev->value;
	free(prev);

	list->size--;
	return tmp;
}

void pushBack(DblLinkedList *list, void *value) {
	Node *tmp = (Node*)malloc(sizeof(Node));
	if (tmp == NULL) {
		exit(3);
	}
	tmp->value = value;
	tmp->next = NULL;
	tmp->prev = list->tail;
	if (list->tail) {
		list->tail->next = tmp;
	}
	list->tail = tmp;

	if (list->head == NULL) {
		list->head = tmp;
	}
	list->size++;
}

void* popBack(DblLinkedList *list) {
	Node *next;
	void *tmp;
	if (list->tail == NULL) {
		exit(4);
	}

	next = list->tail;
	list->tail = list->tail->prev;
	if (list->tail) {
		list->tail->next = NULL;
	}
	if (next == list->head) {
		list->head = NULL;
	}
	tmp = next->value;
	free(next);

	list->size--;
	return tmp;
}


Node* getNth(DblLinkedList *list, size_t index) {
	Node *tmp = list->head;
	size_t i = 0;

	while (tmp && i < index) {
		tmp = tmp->next;
		i++;
	}

	return tmp;
}


void insert(DblLinkedList *list, size_t index, void *value) {
	Node *elm = NULL;
	Node *ins = NULL;
	elm = getNth(list, index);
	if (elm == NULL) {
		exit(5);
	}
	ins = (Node*)malloc(sizeof(Node));
	ins->value = value;
	ins->prev = elm;
	ins->next = elm->next;
	if (elm->next) {
		elm->next->prev = ins;
	}
	elm->next = ins;

	if (!elm->prev) {
		list->head = elm;
	}
	if (!elm->next) {
		list->tail = elm;
	}

	list->size++;
}

void* deleteNth(DblLinkedList *list, size_t index) {
	Node *elm = NULL;
	void *tmp = NULL;
	elm = getNth(list, index);
	if (elm == NULL) {
		exit(5);
	}
	if (elm->prev) {
		elm->prev->next = elm->next;
	}
	if (elm->next) {
		elm->next->prev = elm->prev;
	}
	tmp = elm->value;

	if (!elm->prev) {
		list->head = elm->next;
	}
	if (!elm->next) {
		list->tail = elm->prev;
	}

	free(elm);

	list->size--;

	return tmp;
}

void transfer(DblLinkedList *list, size_t first, size_t second) {
	Node *p = NULL;
	Node *k = NULL;
	Node *temp = NULL;
	p = getNth(list, first);
	k = getNth(list, second);
	if (p->next == k) {
		k->prev = p->prev;
		p->next = k->next;
		if (k->next)
			k->next->prev = p;
		else
			list->tail = p;
		
		if (p->prev)
			p->prev->next = k;
		else
			list->head = k;
		p->prev = k;
		k->next = p;

	}
	else if (p < k) {
		if (p->prev)
			p->prev->next = p->next;
		else list->head = p->next;
		p->prev = k;
		p->next->prev = p->prev;
		p->next = k->next;
		
		k->next = p;
		if (k->next)
			k->next->prev = p;
		else
			list->tail = p;
		
		
		
	}
	else cout << "fail";

}



void printDblLinkedList(DblLinkedList *list, void(*fun)(void*)) {
	Node *tmp = list->head;
	while (tmp) {
		fun(tmp->value);
		tmp = tmp->next;
	}
	printf("\n");
}


void printInt(void *value) {
	printf("%d ", *((byte*)value));
}

DblLinkedList* fromArray(void *arr, size_t n, size_t size) {
	DblLinkedList *tmp = NULL;
	size_t i = 0;
	if (arr == NULL) {
		exit(7);
	}

	tmp = createDblLinkedList();
	while (i < n) {
		pushBack(tmp, ((char*)arr + i*size));
		i++;
	}

	return tmp;
}


int  main() {
	DblLinkedList *list = createDblLinkedList();
	DblLinkedList *zadan2 = createDblLinkedList();
	byte *zadan1 = new byte[N];
	for (int count = 0; count < N; count++)
		zadan1[count] = count*count;
	for (int count = 0; count < N; count++) {
		pushFront(list, &zad1[count]);
	}
	printDblLinkedList(list, printInt);
	cout << "A. "; size_t elem, number, numb_el;
	cout << "enter el who you can isert and where to add ";
	cin >> number_el >> elem;
	int *p1; p1 = &elem;
	insert(list, elem, p1);
	printDblLinkedList(list, printInt);

	cout << "B. ";
	cout << "enter del el"<< endl; size_t el;
	cin >> el;
	deleteNth(list, el);
	printDblLinkedList(list, printInt);

	cout << "C.";
	printf("length %d\n", list->size);

	cout << "D. which to switch";
	size_t ele, ele1;
	cin >> ele >> ele1;
	transfer (list, ele, ele1)
	printDblLinkedList(list, printInt);
	
	deleteDblLinkedList(&list);
	
cout << "\n\nzadanie2\n\n";	
		int ind;
		int s;
		int* p;
		byteq *zad2mas = new byteq[N];
	for (int i = 0; i < 10;i++) {
		cout << "Add or Delete (0 or 1)";
		
		cin >> ind;
		
		if (ind == 0) 
		{
			cin >> s;
			zad2mas[i]=(byteq)s;
			pushFront(zadan2,&zad2mas[i]);
			printDblLinkedList(zadan2, printInt);
		}
		if (ind == 1) {
			popFront(zadan2);
			printDblLinkedList(zadan2, printInt);
		}
	}
	

	deleteDblLinkedList(&zadan2);
//	system("pause");
	return 0;
}
